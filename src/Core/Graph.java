package Core;

/**
 * Klasse zum Erstellen und Behandeln eines Graphen
 * @author Hannes
 *
 */
public class Graph {
	private static int count;
	
	private int id;
	private Node[] nodes;	//Array der Knoten dieses Graphen
	private Edge[] edges;	//Array der Kanten dieses Graphen
	
	/**
	 * Konstruktor der Klasse Graph (erzeugt ein Graph-Objekt)
	 * @param nodesCount Gibt die maximale Anzahl von hinzufuegbaren Knoten an
	 */
	Graph(int nodesCount) {
		//Das Knoten-Array mit uebergebenem Parameter dimensionieren
		nodes = new Node[nodesCount];
		edges = new Edge[nodesCount * nodesCount];
		
		count++;
		this.id = count;
	}
	
	/**
	 * Fuegt einem Graphen einen Knoten hinzu
	 * @param tmpNodeTxt Beschreibt den Text des hinzuzufuegenden Knoten
	 * @return Gibt den hinzugefuegten Knoten zurueck
	 */
	public Node addNode(String tmpNodeTxt) {
		//Neuen Knoten zum Graphen hinzufuegen
		for(int i = 0; i < nodes.length; i++) {
			if(nodes[i] == null) {
				nodes[i] = new Node(tmpNodeTxt);
				nodes[i].randomizePos();
				return nodes[i];
			}
		}
		return null;
	}
	
	/**
	 * Fuegt einem Graphen eine Kante hinzu
	 * @param tmpEdgeWeight Beschreibt die Gewichtung der hinzuzufuegenden Kante
	 * @return Gibt die hinzugefuegte Kante zurueck
	 */
	public Edge addEdge(int tmpEdgeWeight) {
		//Neue Kante zum Graphen hinzufuegen
		for(int i = 0; i < edges.length; i++) {
			if(edges[i] == null) {
				edges[i] = new Edge(tmpEdgeWeight);
				return edges[i];
			}
		}
		return null;
	}
	
	/**
	 * Entfernt einen Knoten von einem Graphen
	 * @param node Beschreibt den zu entfernenden Knoten
	 */
	public void removeNode(Node node) {
		//Knoten vom Graphen abkoppeln (bzw. entfernen)
		for(int i = 0; i < nodes.length; i++) {
			if(nodes[i] == node) {
				nodes[i] = null;
			}
		}
	}
	
	/**
	 * Entfernt eine Kante von einem Graphen
	 * @param edge Beschreibt die zu entfernende Kante
	 */
	public void removeEdge(Edge edge) {
		for(int i = 0; i < edges.length; i++) {
			if(edges[i] == edge) {
				edges[i] = null;
			}
		}
	}
	
	/**
	 * Getter-Methode fuer das Knoten-Array
	 * @return Gibt das Array aller gespeicherten Knoten des Graphen zurueck
	 */
	public Node[] getNodes() {
		return nodes;
	}
	
	/**
	 * Getter-Methode fuer das Kanten-Array
	 * @return Gibt das Array aller gespeicherten Kanten des Graphen zurueck
	 */
	public Edge[] getEdges() {
		return edges;
	}
	
	/**
	 * Getter-Methode fuer einen bestimmten Knoten (aus dem Knoten-Array des Graphen)
	 * @param ID Beschreibt die ID des zu findenden Knoten
	 * @return Gibt das Knoten-Objekt (oder null) aus dem Knoten-Array des Graphen zurueck
	 */
	public Node getNodeByID(int ID) {
		for(int i = 0; i < nodes.length; i++) {
			if(nodes[i] != null) {
				if(nodes[i].getID() == ID) {
					return nodes[i];
				}
			}
		}
		return null;
	}
	
	/**
	 * Getter-Methode fuer alle Kanten (aus dem Kanten-Array des Graphen) die mit einem bestimmten Knoten verbunden sind
	 * @param tmp Beschreibt einen Knoten, der mit der Kante verbunden sein sollte
	 * @return Gibt eine Kante aus dem Kanten-Array des Graphen zurueck
	 */
	public Edge[] getEdgesByNode(Node tmp) {
		Edge[] tmpEdges = new Edge[this.edges.length];
		int tmpCount = 0;
		for(int i = 0; i < edges.length; i++) {
			if(edges[i] != null) {
				Node[] tmpNodes = edges[i].getConnectedNodes();
				if(tmpNodes[0] == tmp || tmpNodes[1] == tmp) {
					tmpEdges[tmpCount] = edges[i];
					tmpCount++;
				}
			}
		}
		return tmpEdges;
	}
	
	/**
	 * Getter-Methode fuer eine bestimmte Kante (aus dem Kanten-Array des Graphen) die mit zwei bestimmten Knoten verbunden ist
	 * @param a	Beschreibt einen Knoten, der mit der Kante verbunden sein sollte
	 * @param b	Beschreibt einen zweiten Knoten, der mit der Kante verbunden sein sollte
	 * @return Gibt eine Kante aus dem Kanten-Array des Graphen zurueck
	 */
	public Edge getEdgeByNodes(Node a, Node b) {
		for(int i = 0; i < edges.length; i++) {
			if(edges[i] != null) {
				Node[] tmpNode = edges[i].getConnectedNodes();
				if((tmpNode[0] == a || tmpNode[0] == b) && (tmpNode[1] == a || tmpNode[1] == b)) {
					return edges[i];
				}
			}
		}
		return null;
	}
	
	/**
	 * Gibt alle Knoten eines Graphen in der Konsole aus (formatiert)
	 */
	public void printNodes() {
		System.out.println("{ KNOTEN DES GRAPHEN }");
		for(int i = 0; i < nodes.length; i++) {
			if(nodes[i] != null) {
				System.out.print("[" + nodes[i].getID() + "; " + nodes[i].getText() + "]");
			}
		}
		System.out.println();
	}
	
	/**
	 * Methode zum Testen ob noch mind. ein Platz im Graphen frei ist
	 * @return Gibt einen Wahrheitswert zurueck
	 */
	public boolean isSpaceAvail() {
		for(int i = 0; i < nodes.length; i++) {
			if(nodes[i] == null) {
				return true;
			}
		}
		return false;
	}
	
	@Override
	public String toString() {
		System.out.println("{ GRAPH }");
		return "-> ID: " + id + "; Anzahl Knoten: " + nodes.length;
	}
}
