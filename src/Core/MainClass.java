package Core;

/**
 * Die Hauptklasse von GraphIt
 * @author Hannes
 * @version 0.9
 *
 */
public class MainClass {
	public static Graph z;	//Graph, mit dem das gesamte Programm ueber gearbeitet wird!
	public static GUI g;	//Aktueller Frame
	
	public static void init() {
		//Konsole vorbereiten
		System.out.println("~[ GraphIt - v0.9 ]~");
		System.out.println("~[ -> : Definitionen/Daten des behandelten Objekts ]~");
		System.out.println("~[ {} : Etwas wurde erstellt ]~");
		System.out.println();
		System.out.println();
	}
	
	public static Graph createRandomGraph(int nodes, int edges) {
		String[] sampleNames = {"Peter", "Anna", "Marie", "Hans", "Gerd", "Fred", "Sophie", "Sara"};
		
		//Graphobjekt erzeugen (Parameter gibt die max. Anzahl der Knoten an)
		Graph g = new Graph(nodes);
		
		//Iterieren bis max. Anzahl der Knoten des Graphen g erreicht
		int x = 0;
		for(int i = 0; i < nodes; i++) {
			g.addNode(sampleNames[(int)(Math.random() * 8)]);	//Knoten erstellen
			x += 100;
		}
		
		Node[] tmpNodes = g.getNodes();
		int randA = 0;
		int randB = 0;
		for(int j = 0; j < edges; j++) {
			Edge tmpEdge = g.addEdge((int)(Math.random() * 10));
			
			//Sichergehen, dass die beiden Zufallsknoten nicht identisch sind!
			while(randA == randB) {
				randA = (int)(Math.random() * nodes);
				randB = (int)(Math.random() * nodes);
			}
			
			//Verbinden der beiden zufaelligen Knoten
			tmpEdge.connectToNodes(tmpNodes[randA], tmpNodes[randB]);
			
			randA = 0;
			randB = 0;
		}
		return g;
	}
	
	public static void main(String[] args) {
		//Initialisieren der Konsolenausgabe des Programms
		init();
		
		//Grafische Oberflaeche starten
		g = new GUI(1000, 500);
	}
}
