package Core;

/**
 * Klasse fuer Knoten eines Graphen
 * @author Hannes
 *
 */
public class Node {
	private static int count;
	
	private int id;
	private String txt;
	private int posX, posY;
	
	/**
	 * Konstruktor der Klasse Node (erzeugt ein Node-Objekt)
	 * @param txt Beschreibt den Text, den der Knoten erhalten soll, um ihn zu identifizieren
	 */
	Node(String txt) {
		this.txt = txt;
		this.posX = (int)(Math.random() * 800);
		this.posY = (int)(Math.random() * 600);
		count++;
		this.id = count;
		System.out.println(this);
	}
	
	/**
	 * Setter-Methode fuer das Setzen der Knoten-ID
	 * @param tmpID Beschreibt die zu setzende ID
	 */
	public void setID(int tmpID) {
		this.id = tmpID;
		count = tmpID;
	}
	
	/**
	 * Setter-Methode fuer das Setzen der Knoten-Position
	 * @param tmpX Beschreibt die horizontale Position des Knoten
	 * @param tmpY Beschreibt die vertikale Position des Knoten
	 */
	public void setPos(int tmpX, int tmpY) {
		this.posX = tmpX;
		this.posY = tmpY;
	}
	
	/**
	 * Setzt die Knoten-Position auf zufaellige Werte
	 */
	public void randomizePos() {
		int fensterX = (int)MainClass.g.getContentPane().getBounds().getWidth();
		int fensterY = (int)MainClass.g.getContentPane().getBounds().getHeight();
		this.posX = (int)(Math.random() * (fensterX - 40));
		this.posY = (int)(Math.random() * (fensterY - 40));
	}
	
	public int 		getPosX() { return this.posX; }
	public int 		getPosY() { return this.posY; }
	public int 		getID()	{ return this.id; }
	public String	getText() { return this.txt; }
	
	@Override
	public String toString() {
		System.out.println("{ KNOTEN }");
		return "-> ID: " + id + "; Text: " + txt;
	}
}