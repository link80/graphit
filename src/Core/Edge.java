package Core;

/**
 * Klasse fuer Kanten eines Graphen
 * @author Hannes
 *
 */
public class Edge {
	private static int count;
	
	private int id;
	private int weight;
	private Node a;
	private Node b;
	
	/**
	 * Erstellen einer gewichteten Kante
	 * @param weight Beschreibt das Gewicht dieser Kante
	 */
	Edge(int weight) {
		this.weight = weight;
		count++;
		this.id = count;
	}
	
	/**
	 * Verbindet die Kante mit zwei Knoten
	 * @param a Erster zu verbindender Knoten
	 * @param b Zweiter zu verbindender Knoten
	 */
	public void connectToNodes(Node a, Node b) {
		this.a = a;
		this.b = b;
	}
	
	/**
	 * Loest die Verbindung der Kante mit zwei Knoten auf
	 * @param a Erster zu loesender Knoten
	 * @param b Zweiter zu loesender Knoten
	 */
	public void disconnectFromNodes(Node a, Node b) {
		this.a = null;
		this.b = null;
	}
	
	/**
	 * Getter-Methode fuer die Gewichtung einer Kante
	 * @return Gibt die Gewichtung der Kante zurueck
	 */
	public int getWeight() {
		return this.weight;
	}
	
	/**
	 * Getter-Methode fuer die verbundenen Knoten der Kante
	 * @return Gibt die verbundenen Knoten in einem Knoten-Array zurueck
	 */
	public Node[] getConnectedNodes() {
		Node[] tmp = new Node[2];
		tmp[0] = this.a;
		tmp[1] = this.b;
		return tmp;
	}
	
	@Override
	public String toString() {
		System.out.println("{ KANTE }");
		return "-> ID: " + this.id + "; Weight: " + this.weight + "; Nodes: " + a + " ; " + b;
	}
}
