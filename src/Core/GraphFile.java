package Core;

import java.io.*;

/**
 * Klasse zum Abspeichern und Laden einer .graph-Datei
 * @author Hannes
 *
 */
public class GraphFile {
	private String destination;
	private BufferedWriter f;
	private BufferedReader r;
	
	/**
	 * Konstruktor der Klasse GraphFile (erzeugt ein GraphFile-Objekt)
	 * @param destination Gibt den Pfad der zu ladenden/speichernden Datei an
	 */
	GraphFile(String destination) {
		this.destination = destination;
	}
	
	/**
	 * Methode zum Speichern von .graph-Dateien
	 * @param tmpGraph Gibt den Graph an, welcher in eine Datei geschrieben werden soll
	 */
	public void save(Graph tmpGraph) {
		Node[] tmpNodes = tmpGraph.getNodes();
		Edge[] tmpEdges = tmpGraph.getEdges();
		
		//SAVE DATA HERE!
		try {
			f = new BufferedWriter(new FileWriter(this.destination));
			f.write("### SAVE FILE GRAPH ###");
			f.newLine();
			f.write(Integer.toString(tmpNodes.length));
			f.newLine();
			
			//Alle Knoten abspeichern
			for(int i = 0; i < tmpNodes.length; i++) {
				if(tmpNodes[i] != null) {
					f.write("1;" + tmpNodes[i].getID() + ";" + tmpNodes[i].getText() + ";" + tmpNodes[i].getPosX() + ";" + tmpNodes[i].getPosY());
					f.newLine();
				}
			}
			
			//Alle Kanten abspeichern
			for(int i = 0; i < tmpEdges.length; i++) {
				if(tmpEdges[i] != null) {
					Node[] conNodes = tmpEdges[i].getConnectedNodes();
					f.write("2;" + conNodes[0].getID() + ";" + conNodes[1].getID() + ";" + tmpEdges[i].getWeight());
					f.newLine();
				}
			}
			
			f.close();
		} catch (IOException e) {
			System.err.println("Fehler beim Schreiben von Daten in Datei!");
		}
	}
	
	/**
	 * Methode zum Laden von .graph-Dateien
	 * @return Gibt ein Graph-Objekt zurück
	 */
	public Graph load() {
		//LOAD DATA HERE!
		try {
			r = new BufferedReader(new FileReader(this.destination));
			r.readLine();	//Header-Zeile (nicht von Bedeutung)
			
			//Auslesen von Anzahl der Knoten (Anzahl der Kanten wird aus Anzahl der Knoten berechnet)
			int countNodes	= Integer.parseInt(r.readLine());
			
			Graph tmpGraph = new Graph(countNodes);
			
			//Auslesen von Knoten und Kanten
			int semic[] = new int[2];
			String tmpS;
			while((tmpS = r.readLine()) != null) {
				//Knoten oder Kante?
				int typeOf		= Integer.parseInt(tmpS.substring(0, 1));
				if(typeOf == 1) {	//Knoten
					semic[0]		= tmpS.indexOf(";", 2);
					int nodeID		= Integer.parseInt(tmpS.substring(2, semic[0]));
					semic[1]		= tmpS.indexOf(";", semic[0] + 1);
					String txtOf	= tmpS.substring(semic[0] + 1, semic[1]);
					semic[0]		= tmpS.indexOf(";", semic[1] + 1);
					int posX		= Integer.parseInt(tmpS.substring(semic[1] + 1, semic[0]));
					int posY		= Integer.parseInt(tmpS.substring(semic[0] + 1));
					
					//Knoten erstellen
					Node tmpNode	= tmpGraph.addNode(txtOf);
					tmpNode.setID(nodeID);
					tmpNode.setPos(posX, posY);
				} else if (typeOf == 2) {	//Kante
					semic[0]		= tmpS.indexOf(";", 2);
					int nodeAID		= Integer.parseInt(tmpS.substring(2, semic[0]));
					semic[1]		= tmpS.indexOf(";", semic[0] + 1);
					int nodeBID		= Integer.parseInt(tmpS.substring(semic[0] + 1, semic[1]));
					int weight 		= Integer.parseInt(tmpS.substring(semic[1] + 1));
					
					//Kanten erstellen und mit ausgelesenen Knoten verbinden
					Edge tmpEdge	= tmpGraph.addEdge(weight);
					tmpEdge.connectToNodes(tmpGraph.getNodeByID(nodeAID), tmpGraph.getNodeByID(nodeBID));
				}
			}
			r.close();
			return tmpGraph;
		} catch (IOException e) {
			System.err.println("Fehler beim Laden von Daten!");
			return null;
		}
	}
}
