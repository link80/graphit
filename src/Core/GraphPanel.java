package Core;

import java.awt.*;
import javax.swing.*;

/**
 * Klasse fuer Inhaltsbereich der GUI
 * @author Hannes
 *
 */
@SuppressWarnings("serial")
public class GraphPanel extends JPanel {
	/**
	 * Konstruktor der Klasse GraphPanel (erzeugt ein JPanel-Objekt)
	 */
	GraphPanel() {
		setSize(1000, 700);
		setOpaque(false);
		setLayout(null);
		setVisible(true);
	}
	
	@Override
	public void paint(Graphics g) {
		super.paintComponents(g);
		
		if(MainClass.z != null) {
			Node[] tmp = MainClass.z.getNodes();
			for(int i = 0; i < tmp.length; i++) {
				if(tmp[i] != null) {
					int tmpX = tmp[i].getPosX();
					int tmpY = tmp[i].getPosY();
					
					g.drawOval(tmpX, tmpY, 40, 40);
					g.drawString(tmp[i].getID() + " ; " + tmp[i].getText(), tmpX, tmpY - 5);
				}
			}
			Edge[] tmp2 = MainClass.z.getEdges();
			for(int i = 0; i < tmp2.length; i++) {
				if(tmp2[i] != null) {
					Node[] connectedNodes = tmp2[i].getConnectedNodes();
					
					//Initialisieren der Positionsvariablen der Knotenpunkte
					int nodeAX = connectedNodes[0].getPosX();
					int nodeAY = connectedNodes[0].getPosY();
					int nodeBX = connectedNodes[1].getPosX();
					int nodeBY = connectedNodes[1].getPosY();
					
					int diffX = (nodeBX + 20) - (nodeAX + 20);
					int diffY = (nodeBY + 20) - (nodeAY + 20);
					
					//Berechnung der Pfeilspitzenausrichtung
					double l = 10.0;
					double a = Math.PI/4 - Math.atan2(((nodeBY + 20) - (nodeAY + 20)), ((nodeBX + 20) - (nodeAX + 20)));
					double c = Math.cos(a) * l;
					double s = Math.sin(a) * l;
					
					//Kante
					g.drawLine(nodeAX + 20, nodeAY + 20, nodeBX + 20, nodeBY + 20);
					//Pfeilspitze
					g.drawLine(nodeBX + 20, nodeBY + 20, (int)((nodeBX + 20) - s), (int)((nodeBY + 20) - c));
					g.drawLine(nodeBX + 20, nodeBY + 20, (int)((nodeBX + 20) - c), (int)((nodeBY + 20) + s));
					//Gewichtung der Kante
					g.drawString(Integer.toString(tmp2[i].getWeight()), nodeAX + (diffX / 2), nodeAY + (diffY / 2));
				}
			}
		}
	}
}