package Core;

import java.awt.*;
import java.awt.event.*;
import java.io.*;

import javax.swing.*;
import javax.swing.filechooser.*;

/**
 * Klasse zum Erstellen einer grafischen Oberflaeche
 * @author Hannes
 *
 */
@SuppressWarnings("serial")
public class GUI extends JFrame {
	/**
	 * Konstruktor der Klasse GUI (erzeugt ein JFrame-Objekt)
	 * @param frameSizeX Gibt die horizontale Groesse des JFrames an
	 * @param frameSizeY Gibt die vertikale Groesse des JFrames an
	 */
	GUI(int frameSizeX, int frameSizeY) {
		//FENSTER
		setTitle("GraphIt");
		setSize(frameSizeX, frameSizeY);
		setLayout(null);
		initMenu();
		initPaint();
		setVisible(true);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}
	
	public void initMenu() {
		//MENUE
		JMenuBar menuBar = new JMenuBar();
		this.setJMenuBar(menuBar);
		
		JMenu fileMenu = new JMenu("Datei");
		menuBar.add(fileMenu);
		
		JMenu nodesMenu = new JMenu("Knoten");
		menuBar.add(nodesMenu);
		
		JMenu edgesMenu = new JMenu("Kanten");
		menuBar.add(edgesMenu);
		
		JMenu pathMenu = new JMenu("Wegfindung");
		menuBar.add(pathMenu);
		
		JMenuItem newGraphItem = new JMenuItem("Neuer Graph ...");
		newGraphItem.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				//Hier Routine um neuen Graph zu erstellen
				JTextField tmpNodes = new JTextField();
				Object[] message = {"Maximale Anzahl der Knoten:", tmpNodes};
				int clicked = JOptionPane.showConfirmDialog(null, message, "Neuer Graph ...", JOptionPane.OK_CANCEL_OPTION);
					
				if(clicked == JOptionPane.OK_OPTION) {
					MainClass.z = new Graph(Integer.parseInt(tmpNodes.getText()));
					repaint();
				}
			}
		});
		fileMenu.add(newGraphItem);
		
		JMenuItem randomGraphItem = new JMenuItem("Zufaelligen Graph erzeugen ...");
		randomGraphItem.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					//Hier zufaelligen Graph erzeugen..
					JTextField tmpNodes = new JTextField();
					JTextField tmpEdges = new JTextField();
					Object[] message = {"Anzahl der Knoten:", tmpNodes, "Anzahl der Kanten:", tmpEdges};
					int clicked = JOptionPane.showConfirmDialog(null, message, "Zufaelligen Graph erzeugen ...", JOptionPane.OK_CANCEL_OPTION);
					
					//Pruefe ob OK geklickt wurde..
					if(clicked == JOptionPane.OK_OPTION) {
						int nodesCount = Integer.parseInt(tmpNodes.getText());
						int edgesCount = Integer.parseInt(tmpEdges.getText());
						
						//Pruefen ob Werte vernuenftig eingegeben wurden
						if(edgesCount <= (nodesCount * nodesCount)) {
							MainClass.z = MainClass.createRandomGraph(nodesCount, edgesCount);
							System.out.println(MainClass.z);
							MainClass.z.printNodes();
							repaint();
						} else {
							//Fehlermeldung ausgeben!
							JOptionPane.showMessageDialog(null, "Falsche Eingabe! Erneut versuchen!", "Fehler", JOptionPane.ERROR_MESSAGE);
						}
					}
				}
		});
		fileMenu.add(randomGraphItem);
		
		fileMenu.addSeparator();
		
		JMenuItem loadItem = new JMenuItem("Graph laden ...", new ImageIcon("img/gameLoad.png"));
		loadItem.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				JFileChooser openGraph = new JFileChooser("Graph laden ...");
				
				openGraph.addChoosableFileFilter(new FileNameExtensionFilter("Graphen (*.graph)", "graph"));
				openGraph.setAcceptAllFileFilterUsed(false);
				
				int returnType = openGraph.showOpenDialog(null);
				
				if(returnType == JFileChooser.APPROVE_OPTION) {
					File destination = openGraph.getSelectedFile();
					GraphFile loadThis = new GraphFile(destination.getAbsolutePath());
					MainClass.z = loadThis.load();
					MainClass.z.printNodes();
					repaint();
				}
			}
		});
		fileMenu.add(loadItem);
		
		JMenuItem saveItem = new JMenuItem("Graph speichern ...", new ImageIcon("img/gameSave.png"));
		saveItem.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				JFileChooser saveGraph = new JFileChooser("Graph speichern ...");
				
				saveGraph.addChoosableFileFilter(new FileNameExtensionFilter("Graphen (*.graph)", "graph"));
				saveGraph.setAcceptAllFileFilterUsed(false);
				
				int returnType = saveGraph.showSaveDialog(null);
				
				if(returnType == JFileChooser.APPROVE_OPTION) {
					File destination = saveGraph.getSelectedFile();
					GraphFile saveThis = new GraphFile(destination.getAbsolutePath() + ".graph");
					saveThis.save(MainClass.z);
				}
			}
		});
		fileMenu.add(saveItem);
		
		fileMenu.addSeparator();
		
		JMenuItem closeItem = new JMenuItem("Beenden");
		closeItem.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				//dispatchEvent(new WindowEvent(super.clone(), WindowEvent.WINDOW_CLOSING));	//new WindowEvent(frame, WindowEvent.WINDOW_CLOSING)
			}
		});
		fileMenu.add(closeItem);
		
		JMenuItem addNodesItem = new JMenuItem("Knoten hinzufuegen ...");
		addNodesItem.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				JTextField tmpTxt = new JTextField();
				Object[] message = {"Text des Knotens:", tmpTxt};
				int clicked = JOptionPane.showConfirmDialog(null, message, "Knoten hinzufuegen ...", JOptionPane.OK_CANCEL_OPTION);
				
				//Pruefe ob der Graph noch Knoten aufnehmen kann..
				if(clicked == JOptionPane.OK_OPTION) {
					if(MainClass.z.isSpaceAvail()) {
						MainClass.z.addNode(tmpTxt.getText());
						repaint();
					} else {
						JOptionPane.showMessageDialog(null, "Der Graph ist voll!", "Nicht moeglich!", JOptionPane.ERROR_MESSAGE);
					}
				}
			}
		});
		nodesMenu.add(addNodesItem);
		
		JMenuItem removeNodesItem = new JMenuItem("Knoten entfernen ...");
		removeNodesItem.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				JTextField tmpTxt = new JTextField();
				Object[] message = {"Welcher Knoten soll entfernt werden?", tmpTxt};
				int clicked = JOptionPane.showConfirmDialog(null, message, "Knoten entfernen ...", JOptionPane.OK_CANCEL_OPTION);
				
				if(clicked == JOptionPane.OK_OPTION) {
					Node tmpNode = MainClass.z.getNodeByID(Integer.parseInt(tmpTxt.getText()));
					if(tmpNode != null) {
						Edge[] tmpEdge = MainClass.z.getEdgesByNode(tmpNode);
						for(int i = 0; i < tmpEdge.length; i++) {
							MainClass.z.removeEdge(tmpEdge[i]);
						}
						MainClass.z.removeNode(tmpNode);
						repaint();
					} else {
						JOptionPane.showMessageDialog(null, "Kein gueltiger Knoten!", "Nicht moeglich!", JOptionPane.ERROR_MESSAGE);
					}
				}
			}
		});
		nodesMenu.add(removeNodesItem);
		
		JMenuItem addEdgeItem = new JMenuItem("Kante hinzufuegen ...");
		addEdgeItem.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				JTextField knotenA = new JTextField();
				JTextField knotenB = new JTextField();
				JTextField gewicht = new JTextField();
				Object[] message = {"Knoten A:", knotenA, "Knoten B:", knotenB, "Gewicht:", gewicht};
				
				int clicked = JOptionPane.showConfirmDialog(null, message, "Kante hinzufuegen ...", JOptionPane.OK_CANCEL_OPTION);
				if(clicked == JOptionPane.OK_OPTION) {
					//OK GEKLICKT!
					Node nodeA = MainClass.z.getNodeByID(Integer.parseInt(knotenA.getText()));
					Node nodeB = MainClass.z.getNodeByID(Integer.parseInt(knotenB.getText()));
					
					Edge tmpEdge = MainClass.z.addEdge(Integer.parseInt(gewicht.getText()));
					tmpEdge.connectToNodes(nodeA, nodeB);
					repaint();
				}
			}
		});
		edgesMenu.add(addEdgeItem);
		
		JMenuItem removeEdgeItem = new JMenuItem("Kante entfernen ...");
		removeEdgeItem.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				JTextField knotenA = new JTextField();
				JTextField knotenB = new JTextField();
				Object[] message = {"Knoten A:", knotenA, "Knoten B:", knotenB};
				
				int clicked = JOptionPane.showConfirmDialog(null, message, "Kante entfernen ...", JOptionPane.OK_CANCEL_OPTION);
				if(clicked == JOptionPane.OK_OPTION) {
					//OK GEKLICKT!
					Edge tmpEdge = MainClass.z.getEdgeByNodes(MainClass.z.getNodeByID(Integer.parseInt(knotenA.getText())), MainClass.z.getNodeByID(Integer.parseInt(knotenB.getText())));
					MainClass.z.removeEdge(tmpEdge);
					repaint();
				}
			}
		});
		edgesMenu.add(removeEdgeItem);
		
		JMenuItem pathItem = new JMenuItem("Weg von A nach B (Tiefensuche)");
		pathMenu.add(pathItem);
		
		JMenuItem pathDeepItem = new JMenuItem("Kuerzester Weg von A nach B (Breitensuche)");
		pathMenu.add(pathDeepItem);
	}
	
	public void initPaint() {
		//ZEICHNEN
		Container panel = new GraphPanel();
		add(panel);
	}
}